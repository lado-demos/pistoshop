var option = null;
option = {
    baudRate: 115200,
    parity: "NONE",
    dataBits: "BITS8",
    stopBits: "1"
}
var printport = "PRINTERPORT1";

const init = () => {
    console.log('Product view has started')
}

const back = () => {
    location.href = 'index.html'
}

const printTicket = () => {
    getVersion();
    openSerialPort();    
}

const getVersion = () => {
    let version = null;
    try {
        version = b2bapis.serialprint.getVersion();
    } catch (e) {
        console.log("[getVersion] call syncFunction exception [" + e.code + "] name: " + e.name + " message: " + e.message);
    }
    if (null !== version) {
        return "[getVersion] call syncFunction type: " + version;
    }
}

const openSerialPort = () => {
    console.log("[open] function call");
    var openStatus = false;

    function onlistener(printSerialData) {
        console.log("Print serial data is " + printSerialData.data + ", Print serial Port is === " + printSerialData.openStatus);
    }
    try {
        openStatus = b2bapis.serialprint.open(printport, option, onlistener);
        if (openStatus) {
            console.log("Success to open print serial port")
            return printData();
        } else {
            console.log("Fail to open print serial port" + result);
        }
    } catch (e) {
        console.log("[open] call syncFunction exception " + e.code + " " + e.errorName + " " + e.errorMessage);
    }
}

const closePrinterPort = () => {
    try {       
        return b2bapis.serialprint.close(printport) ? console.log('Serial port has been closed') : console.log('Fail to close print serial port')
    } catch (e) {
        console.log("[close] call syncFunction exception " + e.code + " " + e.errorName + " " + e.errorMessage);
    }
}

const printData = () => {
    let result = null;
    const data = '48656c6c6f20776f726c64207072696e746572';
    try {
        result = b2bapis.serialprint.writeData(printport, data, data.length);
        console.log("[writeData_0] writeData size is " + result);
        setTimeout(() => closePrinterPort(), 4000)
    } catch (e) {
        console.log("[writeData] call syncFunction exception " + e.code + " " + e.errorName + " " + e.errorMessage);
    }
}