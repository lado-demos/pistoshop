const init = () => {
    console.log('Application has started')
}

const openScanner = () => {
    console.log('scanner started...')
    let Version = null;
    try {
        Version = b2bapis.b2bbarcode.getVersion();
    } catch (e) {
        console.log("[getVersion] call syncFunction exception [" + e.code + "] name: " + e.name + " message: " + e.message);
    }
    if (null !== Version) {
        console.log("[getVersion] call syncFunction type: " + Version);
        b2bapis.b2bbarcode.setScanTimeOut(30);

        function onlistener(barCodeData) {
            console.log("Barcode data is " + barCodeData.data);
            switch (barCodeData.data) {
                case 'corona':
                    location.href = 'corona.html'
                    break;
                case 'modelo':
                    location.href = 'modelo.html'
                    break;
                case 'Pacifico':
                    location.href = 'pacifico.html'
                    break;
            
                default:
                    location.href = 'corona.html'
                    break;
            }
        }
        try {
            b2bapis.b2bbarcode.startScanBarcode(onlistener);
        } catch (e) {
            console.log("[startScanBarcode] call syncFunction exception " + e.code + " " + e.errorName + " " + e.errorMessage);
        }
    }
}